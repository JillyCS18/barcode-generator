from tkinter import *
from tkinter.messagebox import showinfo

class Barcode(Frame):

    EAN_structure = ['LLLLLL', 'LLGLGG',
                     'LLGGLG', 'LLGGGL',
                     'LGLLGG', 'LGGLLG',
                     'LGGGLL', 'LGLGLG',
                     'LGLGGL', 'LGGLGL']
    
    code_digit = [['0001101', '0100111', '1110010'],
                  ['0011001', '0110011', '1100110'],
                  ['0010011', '0011011', '1101100'],
                  ['0111101', '0100001', '1000010'],
                  ['0100011', '0011101', '1011100'],
                  ['0110001', '0111001', '1001110'],
                  ['0101111', '0000101', '1010000'],
                  ['0111011', '0010001', '1000100'],
                  ['0110111', '0001001', '1001000'],
                  ['0001011', '0010111', '1110100']]

    def __init__(self, master):
        Frame.__init__(self, master)
        self.pack()

        self.filename_label = Label(self,
                            font=('Helvetica', 14, 'bold'),
                            text='Save barcode to PS file[eg:EAN13.eps]:')
        self.filename_label.grid(row=0, column=0)

        self.filename_input = Entry(self,
                                font=('Helvetica', 14, 'normal'))               
        self.filename_input.grid(row=1, column=0)

        self.code_label = Label(self,
                            font=('Helvetica', 14, 'bold'),
                            text='Enter code(first 12 decimal digits):')
        self.code_label.grid(row=2, column=0)

        self.code_input = Entry(self,
                                font=('Helvetica', 14, 'normal'))               
        self.code_input.grid(row=3, column=0)

        self.barcode_canvas = Canvas(self, width=500, height=400)                 
        self.barcode_canvas.config(background='white')
        self.barcode_canvas.grid(row=4, column=0)

        self.exit_button = Button(self, text="Exit", command=self.exit_command)
        self.exit_button.grid(row=5, column=0)
        
        self.code_input.bind('<Return>', self.generate_barcode)

    def generate_barcode(self, event):
        self.prepare()
        self.calculate_check_digit()

        first_digit = int(self.code[0])
        first_six_digit_code = self.code[1:7]
        last_six_digit_code = self.code[7:]

        first_EAN_structure = self.EAN_structure[first_digit]

        first_six_digit_encoded_list = []
        last_six_digit_encoded_list = []

        for idx in range(6):
            digit = int(first_six_digit_code[idx])
            if first_EAN_structure[idx] == 'L':
                first_six_digit_encoded_list.append(self.code_digit[digit][0])
            else:
                first_six_digit_encoded_list.append(self.code_digit[digit][1])
        
        for idx in range(6):
            digit = int(last_six_digit_code[idx])
            last_six_digit_encoded_list.append(self.code_digit[digit][2])

        self.encoded_code = ['101'] + first_six_digit_encoded_list + ['01010'] + last_six_digit_encoded_list + ['101']
        print(self.encoded_code)

        self.draw_barcode()

    def prepare(self):
        if ".eps" not in self.filename_input.get():
            showinfo(message="The file should be saved in .eps(PostScript file) extension")
        if len(self.code_input.get()) != 12:
            showinfo(message="The code input must be a 12-digit number")
            

    def calculate_check_digit(self):
        try:
            code_input = self.code_input.get()
            code_odd_idx = [int(code_input[i]) for i in range(0, 12, 2)]
            code_even_idx = [int(code_input[i]) for i in range(1, 12, 2)]

            checksum = (sum(code_even_idx) * 3) + (sum(code_odd_idx))
            if (checksum % 10) != 0:
                self.check_digit = 10 - (checksum % 10)
            else:
                self.check_digit = (checksum % 10)

            self.code = code_input + str(self.check_digit)

        except ValueError:
            showinfo(message='The code input must be in number')

    def draw_barcode(self):
        x = 110.5
        y = 100

        self.barcode_canvas.delete('all')

        self.barcode_canvas.create_text(260, 75, text='EAN-13 Barcode:', font='Arial 16 bold')

        for idx in range(15):
            for code in range(len(self.encoded_code[idx])):
                if self.encoded_code[idx][code] == '1':
                    if idx == 0 or idx == 7 or idx == 14:
                        self.barcode_canvas.create_line(x, y, x, y + 185, width=3, fill='blue')
                        x += 3
                    else:
                        self.barcode_canvas.create_line(x, y, x, y + 170, width=3)
                        x += 3
                else:
                    self.barcode_canvas.create_line(x, y, x, y + 170, width=3, fill='white')
                    x += 3

        self.barcode_canvas.create_text(95, 290, text=(self.code[0]),font='Arial 24 bold')
        self.barcode_canvas.create_text(185, 290, text=(self.code[1:7]), font='Arial 24 bold')
        self.barcode_canvas.create_text(324, 290, text=(self.code[7:]), font='Arial 24 bold')

        check_digit_label = 'Check Digit: ' + str(self.check_digit)
        self.barcode_canvas.create_text(250, 350, text=check_digit_label, font='Arial 16 bold', fill='orange')

        self.barcode_canvas.postscript(file = self.filename_input.get())

    def exit_command(self):
        self.destroy()
        self.quit()

def main():
    root = Tk()
    root.title('EAN-13 Generator By Jilham')

    barcode = Barcode(root)

    root.mainloop()

if __name__ == '__main__':
    main()
